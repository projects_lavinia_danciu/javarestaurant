import Bussiness.*;
import DataLayer.Serializator;
import Presentation.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;


public class Controller  implements Serializable {
    private AdminPage adminPage;
    private ClientPage clientPage;
    private EmployeerPage  pageEmployee=new EmployeerPage();
    private DeliveryService deliveryService;
    private SimpleDateFormat date=new SimpleDateFormat("dd/MM/yyyy");

    public Controller(LoginPage Page, DeliveryService deliveryService, Serializator serializator) {
       String res="";
        this.deliveryService = deliveryService;
        if(serializator.DeserializareComposed()!=null) {
            deliveryService.getCompositelistProducts().addAll(serializator.DeserializareComposed());
            System.out.println(deliveryService.getCompositelistProducts().size());
            System.out.println("Composite products have been deserialized");
        }
        if(serializator.DeserializareBase()!=null) {
            deliveryService.getListBaseProducts().addAll(serializator.DeserializareBase());
            System.out.println(deliveryService.getListBaseProducts().size());
            System.out.println("Base products have been deserialized");
        }
        if(serializator.DeserializareClients()!=null) {
            deliveryService.getClientsList().addAll(serializator.DeserializareClients());
            System.out.println(deliveryService.getClientsList().size());
            System.out.println("Base products have been deserialized");
        }


        deliveryService.getMenuItems().addAll(deliveryService.getCompositelistProducts());
        deliveryService.getMenuItems().addAll(deliveryService.getListBaseProducts());
        Employee employee=new Employee("angajat123","1234","Ana Maria","Cluj");
        Page.CreateButton(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Page.setVisible(false);//se inchide pagina principala
                RegisterPage pageRegister = new RegisterPage();
                pageRegister.setVisible(true);
                pageRegister.RegisterButton(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {

                        if (pageRegister.checkUsername(pageRegister.getUsername(), deliveryService.getClientsList()) == false) {
                           ///////Deserializare
                            Client newClient = new Client(pageRegister.getUsername(), pageRegister.getPassword(), pageRegister.getName(), pageRegister.getAddress(),deliveryService.getClientsList().size()+1);
                            deliveryService.getClientsList().add(newClient);
                            JOptionPane.showMessageDialog(pageRegister, "Client was added succesfully!");
                            ////serializare
                            serializator.SerializareClienti();
                        } else JOptionPane.showMessageDialog(pageRegister, "Client username was already taken!");
                    }
                });
                pageRegister.BackButton(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        pageRegister.setVisible(false);
                        Page.setVisible(true);
                    }
                });
            }
        });

        Page.LoginButton(new ActionListener() {

            int identificare=0;
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Page.dataCombo().equals("EMPLOYEE") && Page.passwordField().equals("1234") && Page.usernameField().equals("angajat1234"))
                {
                   pageEmployee.setVisible(true);

                }
                else

                if (Page.dataCombo().equals("CLIENT") && deliveryService.getClientsList().isEmpty() == false) {
                    if (Page.validateUser(Page.usernameField(), Page.passwordField(), deliveryService.getClientsList(), Page) == 1) {
                        identificare=  deliveryService.searchClient(deliveryService.getClientsList(), Page.usernameField(), Page.passwordField());
                        clientPage = new ClientPage();
                        clientPage.setVisible(true);
                    }

                    clientPage.viewButton(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            TableView<BaseProduct> table = new TableView<BaseProduct>(deliveryService.getListBaseProducts());
                        }
                    });

                    clientPage.view1Button(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            MenuCompositeDesign table = new MenuCompositeDesign(deliveryService);

                        }
                    });
                    clientPage.SearchButton(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {

                           try {
                               List<BaseProduct> ListCriteria=deliveryService.searchByCriteria(clientPage.getTitleJ(),clientPage.getRankJ(),clientPage.getCaloriesJ(),clientPage.getProteinsJ(),clientPage.getFatsJ(),clientPage.getsodiumJ(),clientPage.getPriceJ());
                               TableView<BaseProduct> table = new TableView<>(ListCriteria);
                           }
                           catch(Exception ex)
                            {
                                JOptionPane.showMessageDialog(clientPage,"We don't have product for this criteria");
                            }

                        }
                    });

                    clientPage.OrderButton(new ActionListener() {


                        @Override
                        public void actionPerformed(ActionEvent e) {

                            OrderPage orderPage=new OrderPage(deliveryService);
                            List<Order> orderList=new LinkedList<>();
                            List<MenuItem> buyList=new LinkedList<>();
                            orderPage.addProductButton(new ActionListener() {

                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    MenuItem addToList=deliveryService.checkMenuName((String) orderPage.Combo().getSelectedItem());
                                    ///numara de cate ori a fost comandat
                                    addToList.setCount(addToList.getCount()+1);
                                    buyList.add(addToList);


                                }

                            });

                            orderPage.doneOrderButton(new ActionListener() {

                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    LocalTime localTime = LocalTime.now();
                                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
                                    String time=dtf.format(localTime);
                                    Date data=new Date();
                                    SimpleDateFormat dataFormat=new SimpleDateFormat("MM/dd/yyyy");
                                    String dataComanda= dataFormat.format(data);
                                    Order order=new Order(dataComanda,time);
                                    order.setIdClient(identificare);
                                    order.setIdC(order.getOrderID());
                                    System.out.println(dataComanda);
                                    System.out.println(time);
                                    System.out.println(order.getOrderID());
                                    List<MenuItem> copy=new LinkedList<>();
                                    ///////////aici e ciudat ca o trebuit sa fac neaparat o alta lista
                                    copy.addAll(buyList);
                                    deliveryService.OrderAdd(order,copy);
                                    orderList.add(order);
                                    System.out.println(order.getOrderID()+" VALUE "+order.getOrderPrice());
                                   /* for (Order index:deliveryService.getOrders().keySet())
                                    {
                                        System.out.println(deliveryService.getOrders().get(index)+"key "+index);
                                    }*/
                                    ///mapul face urat la stergere
                                    buyList.clear();
                                    pageEmployee.getText().append(order.update(order.getIdC(),order.getOrderPrice(),order.getOrderDate(),order.getOrderTime()));

                                }
                            });
                        }
                    });

                } else

                    if (Page.dataCombo().equals("ADMINISTRATOR")) {
                    if (Page.validateAdmin(Page.usernameField(), Page.passwordField()) == 1) {
                        Admin admin = new Admin("Admin", "principalAdmin", "Popoviciu Petra", "Cluj 704455");
                        ///Page.setVisible(false);
                        adminPage = new AdminPage();
                        adminPage.setVisible(true);

                        adminPage.viewButton(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                try {
                                    //////////////////////cand se da import se pun  si in meniu
                                    for (BaseProduct index : admin.importProduct()) {
                                        deliveryService.getListBaseProducts().add(index);
                                        deliveryService.getMenuItems().add(index);

                                    }
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                }
                                TableView<BaseProduct> table = new TableView<>(deliveryService.getListBaseProducts());
                                /// Collections.sort(deliveryService.getListBaseProducts());
                                //  Collections.sort(deliveryService.getMenuItems());
                                if (serializator.DeserializareBase() == null) serializator.SerializareBase();


                            }
                        });

                        adminPage.addButton(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                Collections.sort(deliveryService.getListBaseProducts());
                                String title = adminPage.getTitleJ();
                                float rank = Float.parseFloat(adminPage.getRankJ());
                                float proteins = Float.parseFloat(adminPage.getProteinsJ());
                                float calories = Float.parseFloat(adminPage.getCaloriesJ());
                                float fats = Float.parseFloat(adminPage.getFatsJ());
                                float sodium = Float.parseFloat(adminPage.getsodiumJ());
                                float price = Float.parseFloat(adminPage.getPriceJ());
                                if (deliveryService.searchProduct(title) == null) {
                                    BaseProduct newBaseProduct = new BaseProduct(title, rank, calories, proteins, fats, sodium, price);
                                    deliveryService.getListBaseProducts().add(newBaseProduct);
                                    deliveryService.getMenuItems().add(newBaseProduct);
                                    Collections.sort(deliveryService.getListBaseProducts());
                                    ///sorteaza dupa ce adauga in lista de meniuri
                                    Collections.sort(deliveryService.getMenuItems());
                                    TableView<BaseProduct> table = new TableView<>(deliveryService.getListBaseProducts());
                                    JOptionPane.showMessageDialog(adminPage, "Product added!");
                                    serializator.flush("C:\\Users\\asus\\Desktop\\tema4\\text.ser");
                                    serializator.SerializareBase();
                                } else {
                                    JOptionPane.showMessageDialog(adminPage, "Product already added!");
                                    System.out.println(deliveryService.searchProduct(title).getTitle());
                                }


                            }
                        });
                        adminPage.deleteButton(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                String title = adminPage.getTitleJ();
                                if (deliveryService.searchProduct(title) != null) {
                                    deliveryService.getListBaseProducts().remove(deliveryService.searchProduct(title));
                                    JOptionPane.showMessageDialog(adminPage, "Product deleted!");
                                    TableView<BaseProduct> table = new TableView<>(deliveryService.getListBaseProducts());

                                } else JOptionPane.showMessageDialog(adminPage, "Product doesn't exist!");
                                serializator.SerializareBase();
                            }


                        });
                        adminPage.Raport1Button(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                ReportOne raport=new ReportOne();
                                raport.ShowButton(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        raport.getText().setText("");
                                        String start=raport.FieldHourStart();
                                        String end=raport.FieldHourEnd();
                                        deliveryService.Report1(start,end,raport.getText());
                                    }
                                });

                            }
                        });


                        adminPage.Raport2Button(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                 ReportTwo raport=new ReportTwo();
                                  raport.ShowButton(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        raport.getText().setText("");
                                        deliveryService.Report2(Integer.parseInt(raport.FieldNmber()),raport.getText());
                                        TableShow tableView=new TableShow(deliveryService);

                                    }
                                });

                            }
                        });
                        adminPage.Raport4Button(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                RaportFour raportFour=new RaportFour();
                                raportFour.ShowButton(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {

                                         String data=raportFour.FieldNmber1();
                                         deliveryService.Report4(data,raportFour.getText(),raportFour.FieldNmber2());

                                    }
                                });

                            }
                        });

                        adminPage.Raport3Button(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                  RaportThree raportThree=new RaportThree();
                                raportThree.ShowButton(new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        raportThree.getText().setText("");
                                        Integer sum=raportThree.FieldNmber2();
                                        Integer numberOrders=raportThree.FieldNmber1();
                                      for (Client index:deliveryService.getClientsList())
                                        {  ///System.out.println(index.getIdClient());
                                            deliveryService.Report3(index.getIdClient(),sum,numberOrders,raportThree.getText());
                                        }
                                    }
                                });


                            }
                        });



///////////////////////////////////////////////////////
                        adminPage.editButton(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                String title = adminPage.getTitleJ();
                                String rank = adminPage.getRankJ();
                                String proteins = adminPage.getProteinsJ();
                                String calories = adminPage.getCaloriesJ();
                                String fats = adminPage.getFatsJ();
                                String sodium = adminPage.getsodiumJ();
                                String price = adminPage.getPriceJ();
                                if (deliveryService.searchProduct(title) != null) {
                                    deliveryService.editProduct(deliveryService.searchProduct(title), title, rank, proteins, calories, fats, sodium, price);
                                    JOptionPane.showMessageDialog(adminPage, "Product updated!");
                                    serializator.SerializareBase();
                                    TableView<BaseProduct> table = new TableView<>(deliveryService.getListBaseProducts());

                                } else JOptionPane.showMessageDialog(adminPage, "Product doesn't exist!");

                            }
                        });
/////////////////////////////////////////////////////////

                        adminPage.createButton(new ActionListener() {
                            CompositeProduct compositeProduct;

                            @Override
                            public void actionPerformed(ActionEvent e) {
                                CompositeDesign page = new CompositeDesign(deliveryService.getListBaseProducts());
                                page.setVisible(true);
                                compositeProduct = new CompositeProduct("", 0);
                                for (MenuItem index : deliveryService.getListBaseProducts()) {
                                    if (deliveryService.searchProductMenu(index.getTitle()) == null)
                                        deliveryService.getMenuItems().add(index);
                                }

                        /////////////////////////////////////////////////////////////

                        page.addComposite(new ActionListener() {

                            @Override
                            public void actionPerformed(ActionEvent e) {
                                if (page.getNameJ().isEmpty() == false) {
                                    if (page.getNameJ().isEmpty() == false) {
                                        String searched = (String) page.getCombo().getSelectedItem();
                                        compositeProduct.setTitle(page.getNameJ());
                                        if (deliveryService.searchProduct(searched) != null) {
                                            BaseProduct productToAdd = deliveryService.searchProduct(searched);
                                            compositeProduct.addProduct(productToAdd);
                                            compositeProduct.setPrice(compositeProduct.computePrice());
                                        }
                                    } else JOptionPane.showMessageDialog(page, "Empty name for Menu");
                                } else JOptionPane.showMessageDialog(page, "invalid name ");


                            }
                        });
                        //////////////////////////////////////////////////////////////


                        page.doneComposite(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                if (page.getNameJ().isEmpty() == false) {
                                    if (deliveryService.searchMenuName(page.getNameJ()) == 0) {
                                        deliveryService.getMenuItems().add(compositeProduct);
                                        deliveryService.getCompositelistProducts().add(compositeProduct);
                                        compositeProduct = new CompositeProduct("", 0);
                                        serializator.SerializareComposed();
                                    } else JOptionPane.showMessageDialog(page, "Name menu already taken");
                                } else JOptionPane.showMessageDialog(page, "Empty name for Menu");
                                Collections.sort(deliveryService.getMenuItems());

                            }
                        });
                        }

                        });



                    }
                    }
            }
        });
    }
}