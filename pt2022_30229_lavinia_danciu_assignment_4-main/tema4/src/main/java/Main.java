import Bussiness.DeliveryService;
import DataLayer.Serializator;
import Presentation.LoginPage;

import java.io.IOException;

public class Main {

    public static void main(String args[]) {

        LoginPage page1 = new LoginPage();

        DeliveryService deliveryService = new DeliveryService();
        Serializator serializator=new Serializator(deliveryService);
        Controller controller = new Controller(page1,deliveryService,serializator);
        page1.setVisible(true);


    }

}
