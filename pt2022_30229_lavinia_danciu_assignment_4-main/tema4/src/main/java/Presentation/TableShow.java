package Presentation;

import Bussiness.DeliveryService;
import Bussiness.MenuItem;

import javax.swing.*;
import java.util.LinkedList;
import java.util.List;


public class TableShow extends JFrame {
    private JTable table;
    private JScrollPane scrollPane;
    private List<MenuItem> information=new LinkedList<>();
    public  TableShow (DeliveryService delivery){
        setTitle("Show ordered list");
        setSize(800, 400);
        String[] columns = {"Title", "Component","Price" ,"Number oredered"};
        int sizeColumns = columns.length;
        this.information= delivery.getMenuItems();
        int sizeRows = information.size();
        String[][] rows = new String[sizeRows][sizeColumns];
        int i = 0;
        for(MenuItem c :information) {
            if(c.getCount()!=0) {
                if (i < sizeRows) {
                    rows[i][0] = c.getTitle();
                    rows[i][1] = c.toString();
                    rows[i][2] = String.valueOf(c.computePrice());
                    rows[i][3] = String.valueOf(c.getCount());
                    i++;
                }
            }
        }

        table = new JTable(rows,columns);
        scrollPane = new JScrollPane(table);
        add(scrollPane);
        setVisible(true);

    }
}
