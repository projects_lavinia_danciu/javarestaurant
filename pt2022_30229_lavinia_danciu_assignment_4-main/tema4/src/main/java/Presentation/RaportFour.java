package Presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class RaportFour extends JFrame {
    private JTextField spinner;
    private JTextField spinner1;
    private  JPanel panel;
    private  JLabel label;
    private  JLabel label1;
    private JButton show;
    private JScrollPane scroll;



    private JTextArea text;
    public RaportFour() {
        setSize(1200, 900);
        setTitle("Report4");
        setLocationRelativeTo(null);
        text=new JTextArea("Real Time Display \n",10,50);
        text.setBounds(700 ,100,700,700);
        Font font1=new Font(Font.SERIF, Font.PLAIN,  20);
        text.setFont(font1);
        Font font=new Font(Font.SERIF, Font.PLAIN,  50);
        scroll=new JScrollPane(text,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll.setBounds(700,100,700,700);
        show=new JButton("SHOW DATA");
        show.setBackground(Color.getHSBColor(255,83,132));
        show.setFont(new Font("Arial", Font.BOLD, 20));
        show.setBounds(50, 350, 200, 50);
        SpinnerModel value=new SpinnerNumberModel(1,1,24,1);
        SpinnerModel value1=new SpinnerNumberModel(1,1,24,1);
        label=new JLabel("Introduce a date for the order searched");
        label.setBounds(50, 50, 600, 25);
        label.setFont(new Font("Arial", Font.BOLD, 30));
        label1=new JLabel("How many times products are oredered?");
        label1.setBounds(50, 180, 600, 25);
        label1.setFont(new Font("Arial", Font.BOLD, 30));
        panel=new JPanel();
        panel.setLayout(null);
        spinner=new JTextField();
        spinner.setFont(font);
        spinner.setBounds(50, 80, 400, 100);
        spinner1=new JTextField();
        spinner1.setFont(font);
        spinner1.setBounds(50, 210, 400, 100);
        panel.add(spinner);
        panel.add(show);
        panel.add(label);
        panel.add(spinner);
        panel.add(spinner1);
        panel.add(label1);
        this.add(scroll);
        this.add(panel);
        setVisible(true);

    }
    public void setText(JTextArea text) {
        this.text = text;
    }

    public JTextArea getText() {
        return text;
    }
    public String FieldNmber1()
    {
        return spinner.getText();
    }
    public Integer FieldNmber2()
    {
        return Integer.parseInt(spinner1.getText());
    }
    public void ShowButton(ActionListener actionListener) {
        this.show.addActionListener(actionListener);
    }

}
