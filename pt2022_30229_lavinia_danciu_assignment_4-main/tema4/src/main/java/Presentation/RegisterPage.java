package Presentation;

import Bussiness.Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.LinkedList;

public class RegisterPage  extends JFrame {

    private JPanel panel;
    private JButton Register;
    private JTextField username;
    private JTextField password;
    private JTextField name;
    private JTextField address;
    private JLabel labelUsername;
    private JLabel labelPassword;
    private JLabel labelName;
    private JLabel labelAddress;
    private   ImageIcon img;
    private  JButton back;
    public String getUsername() {
        return username.getText();
    }
    public void setUsername(JTextField username) {
        this.username = username;
    }
    public String getPassword() {
        return password.getText();
    }
    public void setPassword(JTextField password) {
        this.password = password;
    }

    @Override
    public String getName() {
        return name.getText();
    }

    public void setName(JTextField name) {
        this.name = name;
    }

    public String getAddress() {
        return address.getText();
    }

    public void setAddress(JTextField address) {
        this.address = address;
    }

    private JLabel label;


    public RegisterPage()
    {
        setSize(1000, 700);
        setTitle("Register Client");
        setLocationRelativeTo(null);
        img=new ImageIcon();
        back=new JButton("BACK TO LOGIN ");
        username=new JTextField();
        username.setBounds(100, 80, 300, 50);
        password=new JTextField();
        password.setBounds(100, 200, 300, 50);
        name=new JTextField();
        name.setBounds(100, 300, 300, 50);
        address=new JTextField();
        address.setBounds(100, 400, 300, 50);
        Register = new JButton("REGISTER");
        panel= new JPanel();
        panel.setLayout(null);//important
        img=new ImageIcon(getClass().getResource("register.jpg"));
        label=new JLabel(img);
        label.setBounds(450,-10,800,700);
        Register.setBackground(Color.getHSBColor(255,83,132));
        Register.setFont(new Font("Arial", Font.BOLD, 20));
        Register.setBounds(100, 480, 200, 50);
        back.setBackground(Color.getHSBColor(255,83,132));
        back.setFont(new Font("Arial", Font.BOLD, 20));
        back.setBounds(100, 550, 200, 50);
        labelUsername= new JLabel("USERNAME:");
        labelUsername.setBounds(100, 50, 200, 25);
        labelUsername.setFont(new Font("Arial", Font.BOLD, 20));
        labelPassword= new JLabel("PASSWORD:");
        labelPassword.setBounds(100, 180, 200, 25);
        labelPassword.setFont(new Font("Arial", Font.BOLD, 20));
        labelName= new JLabel("NAME:");
        labelName.setBounds(100, 280, 200, 25);
        labelName.setFont(new Font("Arial", Font.BOLD, 20));
        labelAddress= new JLabel("ADDRESS:");
        labelAddress.setBounds(100, 380, 200, 25);
        labelAddress.setFont(new Font("Arial", Font.BOLD, 20));
        panel.add(back);
        panel.add(Register);
        panel.add(username);
        panel.add(password);
        panel.add(name);
        panel.add(address);
        panel.add(labelUsername);
        panel.add(labelPassword);
        panel.add(labelName);
        panel.add(labelAddress);
        panel.add(label);
        this.add(panel);
        this.setVisible(true);


    }
    public void RegisterButton(ActionListener actionListener) {
        this.Register.addActionListener(actionListener);

    }
    public void BackButton(ActionListener actionListener) {
        this.back.addActionListener(actionListener);

    }
    public boolean checkUsername(String Username , LinkedList<Client> list)
    {  if(list.isEmpty() ) return  false;
        else {
        for (Client index : list) {
            if (index.getUsername().equals(Username) == true) {
                return true;
            }
        }
      }
        return false;
    }


}
