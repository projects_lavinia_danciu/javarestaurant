package Presentation;

import Bussiness.BaseProduct;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.LinkedList;

public class CompositeDesign extends JFrame{
    JComboBox select;
    JLabel label;
    JPanel pannel;
    JLabel labelName;
    JTextField Name;
    JButton addComposite;
    JButton done;

    public CompositeDesign(LinkedList<BaseProduct>list) {
        setSize(1200, 800);
        setTitle("Create Menu");
        setLocationRelativeTo(null);
        pannel=new JPanel();
        done=new JButton();
        select=new JComboBox();
        Name=new JTextField();
        for(BaseProduct index:list)
            select.addItem(index.getTitle());
        label=new JLabel("Name of the composite Product");
        label.setFont(new Font("Arial", Font.BOLD, 30));
        label.setBounds(100,100,500,50);
        Name.setBounds(100,150,500,50);
        labelName=new JLabel("Select the Base products");
        labelName.setFont(new Font("Arial", Font.BOLD, 30));
        labelName.setBounds(100,200,500,50);
        select.setBounds(100,250,400,50);
        addComposite=new JButton("ADD");
        addComposite.setBackground(Color.getHSBColor(255,83,132));
        addComposite.setFont(new Font("Arial", Font.BOLD, 20));
        addComposite.setBounds(100, 300, 200, 50);
        done=new JButton("DONE");
        done.setBackground(Color.getHSBColor(255,83,132));
        done.setFont(new Font("Arial", Font.BOLD, 20));
        done.setBounds(100, 350, 200, 50);
        pannel.setLayout(null);
        pannel.add(label);
        pannel.add(Name);
        pannel.add(labelName);
        pannel.add(select);
        pannel.add(addComposite);
        pannel.add(done);
        this.add(pannel);


    }

    public void addComposite(ActionListener actionListener) {
        this.addComposite.addActionListener(actionListener);
    }
    public void doneComposite(ActionListener actionListener) {
        this.done.addActionListener(actionListener);
    }
    public JComboBox getCombo()
    {
        return select;
    }
    public String getNameJ()
    {
        return Name.getText();
    }


}
