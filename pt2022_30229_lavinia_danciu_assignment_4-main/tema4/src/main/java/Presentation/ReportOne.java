package Presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class ReportOne extends JFrame {
   private JTextField  textfieldOne;
    private JTextField  textfieldTwo;
    private  JPanel panel;
    private JLabel startHour;
    private JLabel endHour;
    private JButton show;
    private JTextArea text;
    private JScrollPane scroll;


    public ReportOne() {

        setSize(1200, 900);
        setTitle("Report1");
        setLocationRelativeTo(null);
        text=new JTextArea("Real Time Display \n",10,50);
        text.setBounds(300 ,100,800,700);
        Font font1=new Font(Font.SERIF, Font.PLAIN,  20);
        text.setFont(font1);
        scroll=new JScrollPane(text,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll.setBounds(300,100,800,700);
        startHour=new JLabel("INTRODUCE \n THE START HOUR");
        startHour.setBounds(50, 50, 500, 25);
        startHour.setFont(new Font("Arial", Font.BOLD, 30));
        endHour=new JLabel("INTRODUCE \nTHE END HOUR");;
        endHour.setBounds(50, 200, 500, 25);
        endHour.setFont(new Font("Arial", Font.BOLD, 30));
        panel=new JPanel();
        panel.setLayout(null);
        textfieldOne=new JTextField();
        textfieldTwo=new JTextField();
        Font font=new Font(Font.SERIF, Font.PLAIN,  50);
        show=new JButton("SHOW DATA");
        show.setBackground(Color.getHSBColor(255,83,132));
        show.setFont(new Font("Arial", Font.BOLD, 20));
        show.setBounds(50, 350, 200, 50);
        textfieldOne.setFont(font);;
        textfieldOne.setBounds(50, 80, 100, 100);
        textfieldTwo.setBounds(50, 230, 100, 100);
        textfieldTwo.setFont(font);
        panel.add(textfieldOne);
        panel.add(show);
        panel.add(startHour);
        panel.add(endHour);
        panel.add( textfieldTwo);
        panel.add(textfieldOne);
        this.add(scroll);
        this.add(panel);
        setVisible(true);


    }
    public String  FieldHourStart()
    {
        return textfieldOne.getText();
    }
    public String  FieldHourEnd()
    {
        return textfieldTwo.getText();
    }
    public void ShowButton(ActionListener actionListener) {
        this.show.addActionListener(actionListener);
    }
    public JTextArea getText() {
        return text;
    }

}
