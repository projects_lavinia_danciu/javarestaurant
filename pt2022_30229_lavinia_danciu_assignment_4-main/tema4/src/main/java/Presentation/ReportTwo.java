package Presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public  class ReportTwo extends JFrame {
    private JTextField  textfieldOne;
    private  JPanel panel;
    private  JLabel label;
    private JButton show;
    private JScrollPane scroll;



    private JTextArea text;
    public ReportTwo() {
        setSize(1200, 900);
        setTitle("Report2");
        setLocationRelativeTo(null);
        text=new JTextArea("Real Time Display \n",10,50);
        text.setBounds(300 ,100,800,700);
        Font font1=new Font(Font.SERIF, Font.PLAIN,  20);
        text.setFont(font1);
        Font font=new Font(Font.SERIF, Font.PLAIN,  50);
        scroll=new JScrollPane(text,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll.setBounds(300,100,1000,700);
        show=new JButton("SHOW DATA");
        show.setBackground(Color.getHSBColor(255,83,132));
        show.setFont(new Font("Arial", Font.BOLD, 20));
        show.setBounds(50, 350, 200, 50);
        label=new JLabel("INTRODUCE THE NUMBER");
        label.setBounds(50, 50, 500, 25);
        label.setFont(new Font("Arial", Font.BOLD, 30));
        panel=new JPanel();
        panel.setLayout(null);
        textfieldOne=new JTextField();
        textfieldOne.setFont(font);
        textfieldOne.setBounds(50, 80, 100, 100);
        panel.add( textfieldOne);
        panel.add(show);
        panel.add(label);
        panel.add( textfieldOne);
        this.add(scroll);
        this.add(panel);
        setVisible(true);

    }
    public void setText(JTextArea text) {
        this.text = text;
    }

    public JTextArea getText() {
        return text;
    }
    public String FieldNmber()
    {
        return textfieldOne.getText();
    }
    public void ShowButton(ActionListener actionListener) {
        this.show.addActionListener(actionListener);
    }

}
