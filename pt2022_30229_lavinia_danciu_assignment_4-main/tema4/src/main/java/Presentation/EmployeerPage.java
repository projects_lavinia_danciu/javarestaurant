package Presentation;

import javax.swing.*;
import java.awt.*;

public class EmployeerPage extends JFrame {
    private JTextArea text;
    private JScrollPane scroll;
    private JPanel panel;


    public EmployeerPage()
    {
        setSize(1000, 1000);
        setTitle("Admin Page");
        setLocationRelativeTo(null);
        text=new JTextArea("Real Time Display \n",10,50);
        text.setBounds(100 ,100,800,700);
        Font font1=new Font(Font.SERIF, Font.PLAIN,  20);
        text.setFont(font1);
        scroll=new JScrollPane(text,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll.setBounds(100,100,800,700);
        panel=new JPanel();
        this.add(scroll);
        this.add(panel);

    }
    public JTextArea getText() {
        return text;
    }

}




