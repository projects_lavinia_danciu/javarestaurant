package Presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class AdminPage extends JFrame {
    private JLabel label1;
    private JLabel titleL;
    private JLabel ratingL;
    private JLabel caloriesL;
    private JLabel proteinsL;
    private JLabel fatsL;
    private JLabel sodiumL;
    private JLabel priceL;
    private JTextField title;
    private JTextField rating;
    private JTextField calories;
    private JTextField proteins;
    private JTextField fats;
    private JTextField sodium;
    private JTextField price;
    private JPanel panel;
    private JButton delete;
    private JButton create;
    private JButton edit;
    private JButton add;
    private JButton view;
    private JButton back;
    private  JButton Report1;
    private  JButton Report2;
    private  JButton Report3;
    private  JButton Report4;



    public AdminPage() {
        setSize(1200, 800);
        setTitle("Admin Page");
        setLocationRelativeTo(null);
        label1 = new JLabel("MANAGE PRODUCT BY ATRIBUTE");
        label1.setBounds(100, 50, 400, 25);
        label1.setFont(new Font("Arial", Font.BOLD, 20));
        titleL = new JLabel("TITLE PRODUCT");
        titleL.setBounds(100, 90, 400, 25);
        titleL.setFont(new Font("Arial", Font.BOLD, 20));
        title = new JTextField();
        title.setBounds(100, 120, 300, 40);
        ///
        ratingL = new JLabel("RATING");
        ratingL.setBounds(100, 170, 300, 25);
        ratingL.setFont(new Font("Arial", Font.BOLD, 20));
        rating = new JTextField();
        rating.setBounds(100, 200, 300, 40);
        ///
        caloriesL = new JLabel("CALORIES");
        caloriesL.setBounds(100, 250, 300, 25);
        caloriesL.setFont(new Font("Arial", Font.BOLD, 20));
        calories = new JTextField();
        calories.setBounds(100, 280, 300, 40);
        ///
        proteinsL = new JLabel("PROTEINS");
        proteinsL.setBounds(100, 330, 300, 25);
        proteinsL.setFont(new Font("Arial", Font.BOLD, 20));
        proteins = new JTextField();
        proteins.setBounds(100, 360, 300, 40);
        ///
        fatsL = new JLabel("FATS");
        fatsL.setBounds(100, 410, 300, 25);
        fatsL.setFont(new Font("Arial", Font.BOLD, 20));
        fats = new JTextField();
        fats.setBounds(100, 440, 300, 40);
        ///
        sodiumL = new JLabel("SODIUM");
        sodiumL.setBounds(100, 490, 300, 25);
        sodiumL.setFont(new Font("Arial", Font.BOLD, 20));
        sodium = new JTextField();
        sodium.setBounds(100, 520, 300, 40);
        //
        priceL = new JLabel("PRICE");
        priceL.setBounds(100, 570, 300, 25);
        priceL.setFont(new Font("Arial", Font.BOLD, 20));
        price = new JTextField();
        price.setBounds(100, 610, 300, 40);
        panel = new JPanel();
        panel.setLayout(null);//important
        ///
        delete = new JButton("DELETE");
        delete.setBackground(Color.getHSBColor(100, 500, 98));
        delete.setFont(new Font("Arial", Font.BOLD, 20));
        delete.setBounds(50, 660, 150, 50);
        edit = new JButton("EDIT");
        edit.setBackground(Color.getHSBColor(100, 500, 98));
        edit.setFont(new Font("Arial", Font.BOLD, 20));
        edit.setBounds(200, 660, 150, 50);
        add = new JButton("ADD");
        add.setBackground(Color.getHSBColor(100, 500, 98));
        add.setFont(new Font("Arial", Font.BOLD, 20));
        add.setBounds(350, 660, 150, 50);
        view = new JButton("IMPORT PRODUCT");
        view.setBackground(Color.getHSBColor(100, 500, 98));
        view.setFont(new Font("Arial", Font.BOLD, 20));
        view.setBounds(500, 660, 300, 50);
        create = new JButton("CREATE");
        create .setBackground(Color.getHSBColor(100, 500, 98));
        create .setFont(new Font("Arial", Font.BOLD, 20));
        create .setBounds(800, 660, 150, 50);
        ///////////
        Report1 = new JButton("Report1");
        Report1.setBackground(Color.getHSBColor(100, 640, 98));
        Report1.setFont(new Font("Arial", Font.BOLD, 20));
        Report1.setBounds(500, 100, 150, 50);
        ////////////////
        Report2 = new JButton("Report2");
        Report2.setBackground(Color.getHSBColor(100, 610, 98));
        Report2.setFont(new Font("Arial", Font.BOLD, 20));
        Report2.setBounds(500, 200, 150, 50);
        ////////////////
        Report3 = new JButton("Report3");
        Report3.setBackground(Color.getHSBColor(100, 600, 98));
        Report3.setFont(new Font("Arial", Font.BOLD, 20));
        Report3.setBounds(500, 300, 150, 50);
        ///////////////
        Report4 = new JButton("Report4");
        Report4.setBackground(Color.getHSBColor(100, 670, 98));
        Report4.setFont(new Font("Arial", Font.BOLD, 20));
        Report4.setBounds(500, 400, 150, 50);
        panel.add(title);
        panel.add(caloriesL);
        panel.add(calories);
        panel.add(label1);
        panel.add(titleL);
        panel.add(rating);
        panel.add(ratingL);
        panel.add(proteinsL);
        panel.add(proteins);
        panel.add(fats);
        panel.add(fatsL);
        panel.add(sodiumL);
        panel.add(sodium);
        panel.add(priceL);
        panel.add(price);
        panel.add(add);
        panel.add(delete);
        panel.add(edit);
        panel.add(view);
        panel.add( create );
        panel.add(Report1);
        panel.add(Report2);
        panel.add(Report3);
        panel.add(Report4);

        this.add(panel);
    }

    public void viewButton(ActionListener actionListener) {
        this.view.addActionListener(actionListener);
    }

    public void Raport1Button(ActionListener actionListener) {
        this.Report1.addActionListener(actionListener);
    }

    public void Raport2Button(ActionListener actionListener) {
        this.Report2.addActionListener(actionListener);
    }
    public void Raport3Button(ActionListener actionListener) {
        this.Report3.addActionListener(actionListener);
    }
    public void Raport4Button(ActionListener actionListener) {
        this.Report4.addActionListener(actionListener);
    }
    public void addButton(ActionListener actionListener) {
        this.add.addActionListener(actionListener);
    }
    public void deleteButton(ActionListener actionListener) {
        this.delete.addActionListener(actionListener);
    }
    public void editButton(ActionListener actionListener) {
        this.edit.addActionListener(actionListener);
    }
    public void createButton(ActionListener actionListener) {
        this.create.addActionListener(actionListener);
    }
    public String getTitleJ()
    {
        return title.getText();
    }
    public String getRankJ()
    {
        return rating.getText();
    }
    public String getCaloriesJ()
    {
        return calories.getText();
    }
    public String getProteinsJ()
    {
        return proteins.getText();
    }
    public String getFatsJ()
    {
        return fats.getText();
    }
    public String getsodiumJ()
    {
        return sodium.getText();
    }

    public String getPriceJ()
    {
        return price.getText();
    }




}
