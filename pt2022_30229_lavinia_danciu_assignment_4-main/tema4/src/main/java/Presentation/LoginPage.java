package Presentation;

import Bussiness.Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.LinkedList;

public class LoginPage  extends JFrame {

    private JPanel panel;
    private JButton Login;
    private JButton Create;
    private   ImageIcon img;
    private  JLabel label;
    private  JLabel labelUser;
    private  JLabel labelPass;
    private JTextField username;
    private JTextField password;
    private  JPanel pannelnew;
    private JComboBox role;

    public LoginPage()
    {
        setSize(1000, 700);
        String type[]={"ADMINISTRATOR","CLIENT","EMPLOYEE"};
        setTitle("Restaurant");
        setLocationRelativeTo(null);
        labelUser= new JLabel("USERNAME:");
        labelUser.setBounds(100, 50, 200, 25);
        labelUser.setFont(new Font("Arial", Font.BOLD, 20));
        labelPass= new JLabel("PASSWORD:");
        labelPass.setBounds(100, 130, 200, 25);
        labelPass.setFont(new Font("Arial", Font.BOLD, 20));
        role=new JComboBox(type);
        role.setBounds(100,250,200,50);
        Login = new JButton("LOGIN");
        username=new JTextField();
        username.setBounds(100, 70, 200, 50);
        password=new JTextField();
        password.setBounds(100, 160, 200, 50);
        Create = new JButton("CREATE");
        panel= new JPanel();
        pannelnew=new JPanel();
        img=new ImageIcon(getClass().getResource("restaurant1.png"));
        label=new JLabel(img);
        label.setSize(700,700);
        panel.setLayout(null);//important
        pannelnew.setLayout(null);//important
        Login.setBackground(Color.getHSBColor(255,83,132));
        Login.setFont(new Font("Arial", Font.BOLD, 20));
        Login.setBounds(750, 40, 200, 50);
        Create.setBackground(Color.getHSBColor(100,200,98));
        Create.setFont(new Font("Arial", Font.BOLD, 20));
        Create.setBounds(750, 120, 200, 50);
        panel.add(Create);
        panel.add(Login);
        pannelnew.setBounds(100,100,400,400);
        pannelnew.add(username);
        pannelnew.add(password);
        pannelnew.add(labelUser);
        pannelnew.add(labelPass);
        pannelnew.add(role);
        label.add(pannelnew);
        panel.add(label);
        this.add(panel);
    }



    public void CreateButton(ActionListener actionListener) {
        this.Create.addActionListener(actionListener);

    }



    public void LoginButton(ActionListener actionListener) {
        this.Login.addActionListener(actionListener);
    }
    public int validateUser(String username, String password, LinkedList<Client> list, LoginPage Page) {
        int gasit1 = 0, gasit2 = 0;
        if (username.isEmpty()) JOptionPane.showMessageDialog(Page, "Empty Username");
        else if (password.isEmpty()) JOptionPane.showMessageDialog(Page, "Empty password");
        else {
            for (Client index : list) {
                if (index.getUsername().equals(username) == true) {
                    gasit1 = 1;
                    if (index.getPassword().equals(password) == true) {
                        JOptionPane.showMessageDialog(Page, "Succesfull registration");
                        gasit2 = 1;
                        return  gasit2;}
                }
            }
            if (gasit1 == 0) {
                JOptionPane.showMessageDialog(Page, "Username not found!");
                return gasit1;
            }
            else if (gasit2 == 0)
            { JOptionPane.showMessageDialog(Page, "Invalid Password");
                return gasit2;
            }
        }
        return 0;
    }



    public int validateAdmin(String username, String password)
    {
        if(username.isEmpty() )
        {
            JOptionPane.showMessageDialog(this, "Empty Username Admin");
            return 0;
        }
        else
        if(password.isEmpty() ) {
            JOptionPane.showMessageDialog(this, "Empty Password Admin");
            return 0;
        }
        else
        if (username.equals("Admin") == false) {
            JOptionPane.showMessageDialog(this, "Invalid Username Admin");
            return 0;
        }
        else
        if (password.equals("principalAdmin") == false) {
            JOptionPane.showMessageDialog(this, "Invalid Password Admin");
            return 0;
        }
        else JOptionPane.showMessageDialog(this, "Succesfully Admin Registration");


        return 1;

    }
    public String dataCombo()
    {
        return (String) role.getSelectedItem();
    }

    public String  usernameField()
    {
        return username.getText();
    }
    public String  passwordField()
    {
        return password.getText();
    }
}
