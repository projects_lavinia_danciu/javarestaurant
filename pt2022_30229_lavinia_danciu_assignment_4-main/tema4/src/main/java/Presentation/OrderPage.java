package Presentation;

import Bussiness.DeliveryService;
import Bussiness.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class OrderPage extends JFrame{
   private JComboBox select;
   private JPanel pannel;
   private JButton doneOrder;
   private JButton addProduct;
   private JLabel label;
    public  OrderPage(DeliveryService deliveryService) {
        setSize(1200, 800);
        setTitle("Create Menu");
        setLocationRelativeTo(null);
        pannel = new JPanel();
        pannel.setLayout(null);
        label= new JLabel("Add the product you want to order:");
        label.setBounds(100, 100, 500, 30);
        label.setFont(new Font("Arial", Font.BOLD, 30));
        doneOrder = new JButton("DONE");
        addProduct=new JButton("ADD");
        select = new JComboBox();
        select.setBounds(100,150,400,50);
        addProduct.setBounds(510,150,150,50);
        doneOrder.setBounds(670,150,150,50);
        for (MenuItem index : deliveryService.getMenuItems())
            select.addItem(index.getTitle());
        pannel.add(doneOrder);
        pannel.add(addProduct);
        pannel.add(label);
        pannel.add(select);
        pannel.add(addProduct);
        this.add(pannel);
        this.setVisible(true);
    }
    public void addProductButton(ActionListener actionListener) {
        this.addProduct.addActionListener(actionListener);

    }
    public void doneOrderButton(ActionListener actionListener) {
        this.doneOrder.addActionListener(actionListener);

    }
    public JComboBox Combo ()
    {
        return select;
    }


}

