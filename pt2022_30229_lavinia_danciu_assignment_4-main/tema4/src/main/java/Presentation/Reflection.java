package Presentation;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class Reflection<T> {

    public String[] fetchColumns(List<T> objects) {
        List<T> genericList = new ArrayList<>();
        Object obiect= objects.get(0);
        for (Field field : obiect.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            Object value;
            try {
                value = field.getName();
                genericList.add((T) value);

            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        int sizeColumns = genericList.size();
        String[] columns = new String[sizeColumns];
        for(int i = 0; i < sizeColumns; i++){
            {
                columns[i] = (String) genericList.get(i);

            }

        }

        return columns;
    }


    public String[][] fetchRows(List<T> objects) {
        int sizeRows = objects.size();
        int sizeColumns = this.fetchColumns(objects).length;
        String[][] rows = new String[sizeRows][sizeColumns];

        int i = 0;
        for(T object : objects){
            int j = 0;
            for (Field field : object.getClass().getDeclaredFields())
            {
                field.setAccessible(true);
                Object value;
                try{
                    value = field.get(object);
                    rows[i][j] = value.toString();
                    j++;
                }catch(IllegalArgumentException e){
                    e.printStackTrace();
                }catch(IllegalAccessException e){
                    e.printStackTrace();
                }
            }
            i++;
        }
        return rows;
    }

}

