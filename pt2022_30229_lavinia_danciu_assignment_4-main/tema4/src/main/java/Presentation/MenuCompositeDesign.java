package Presentation;

import Bussiness.CompositeProduct;
import Bussiness.DeliveryService;

import javax.swing.*;
import java.util.LinkedList;

public class MenuCompositeDesign extends JFrame {
    private JTable table;
    private JScrollPane scrollPane;
    private LinkedList<CompositeProduct> information=new LinkedList<>();
    public MenuCompositeDesign (DeliveryService delivery){
        setTitle("MENU");
        setSize(800, 400);
        String[] columns = {"Title", "Component", "Final price"};
        int sizeColumns = columns.length;
        this.information= delivery.getCompositelistProducts();
        for (CompositeProduct index:information)
            System.out.println(index.toString());
        int sizeRows = information.size();
        String[][] rows = new String[sizeRows][sizeColumns];
        int i = 0;
        for(CompositeProduct c :information) {
            if (i < sizeRows) {
                rows[i][0] = c.getTitle();
                rows[i][1]=c.toString();
                rows[i][2] = String.valueOf(c.computePrice());
                i++;
            }
        }

        table = new JTable(rows,columns);
        scrollPane = new JScrollPane(table);
        add(scrollPane);
        setVisible(true);
        for (CompositeProduct index:information)
            System.out.println(index.toString());
    }
}
