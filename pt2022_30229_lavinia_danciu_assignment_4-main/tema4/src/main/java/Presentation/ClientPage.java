package Presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class ClientPage extends JFrame
{
    private JLabel label1;
    private JLabel titleL;
    private JLabel ratingL;
    private JLabel caloriesL;
    private JLabel proteinsL;
    private JLabel fatsL;
    private JLabel sodiumL;
    private JLabel priceL;
    private JTextField title;
    private JTextField rating;
    private JTextField calories;
    private JTextField proteins;
    private JTextField fats;
    private JTextField sodium;
    private JTextField price;
    private JPanel panel;
    private JButton search;
    private JButton view;
    private JButton view1;
    private JButton makeOrder;

    public ClientPage()
    {   setSize(1200, 800);
        setTitle("Client");
        setLocationRelativeTo(null);
        label1= new JLabel("SEARCH PRODUCT BY ATRIBUTE");
        label1.setBounds(100, 50, 400, 25);
        label1.setFont(new Font("Arial", Font.BOLD, 20));
        titleL= new JLabel("TITLE PRODUCT");
        titleL.setBounds(100, 90, 400, 25);
        titleL.setFont(new Font("Arial", Font.BOLD, 20));
        title=new JTextField();
        title.setBounds(100, 120, 300, 40);
        ///
        ratingL= new JLabel("RATING");
        ratingL.setBounds(100, 170, 300, 25);
        ratingL.setFont(new Font("Arial", Font.BOLD, 20));
        rating=new JTextField();
        rating.setBounds(100, 200, 300, 40);
        ///
        caloriesL= new JLabel("CALORIES");
        caloriesL.setBounds(100, 250, 300, 25);
        caloriesL.setFont(new Font("Arial", Font.BOLD, 20));
        calories=new JTextField();
        calories.setBounds(100, 280, 300, 40);
        ///
        proteinsL= new JLabel("PROTEINS");
        proteinsL.setBounds(100, 330, 300, 25);
        proteinsL.setFont(new Font("Arial", Font.BOLD, 20));
        proteins=new JTextField();
        proteins.setBounds(100, 360, 300, 40);
        ///
        fatsL= new JLabel("FATS");
        fatsL.setBounds(100, 410, 300, 25);
        fatsL.setFont(new Font("Arial", Font.BOLD, 20));
        fats=new JTextField();
        fats.setBounds(100, 440, 300, 40);
        ///
        sodiumL= new JLabel("SODIUM");
        sodiumL.setBounds(100, 490, 300, 25);
        sodiumL.setFont(new Font("Arial", Font.BOLD, 20));
        sodium=new JTextField();
        sodium.setBounds(100, 520, 300, 40);
        //
        priceL= new JLabel("PRICE");
        priceL.setBounds(100, 570, 300, 25);
        priceL.setFont(new Font("Arial", Font.BOLD, 20));
        price=new JTextField();
        price.setBounds(100, 610, 300, 40);
        panel= new JPanel();
        panel.setLayout(null);//important
        ///
        search=new JButton("SEARCH");
        view=new JButton("VIEW BASIC PRODUCT");
        view1=new JButton("VIEW PREMIU MENU");
        makeOrder=new JButton("MAKE ORDER");
        search.setBackground(Color.getHSBColor(100,200,98));
        search.setFont(new Font("Arial", Font.BOLD, 20));
        search.setBounds(100, 660, 150, 50);
        view.setBackground(Color.getHSBColor(100,500,98));
        view.setFont(new Font("Arial", Font.BOLD, 20));
        view.setBounds(250, 660, 300, 50);
        view1.setBackground(Color.getHSBColor(100,500,98));
        view1.setFont(new Font("Arial", Font.BOLD, 20));
        view1.setBounds(550, 660, 250, 50);
        makeOrder.setBackground(Color.getHSBColor(100,200,98));
        makeOrder.setFont(new Font("Arial", Font.BOLD, 20));
        makeOrder.setBounds(800, 660, 300, 50);
        panel.add(title);
        panel.add(caloriesL);
        panel.add(calories);
        panel.add(label1);
        panel.add(titleL);
        panel.add(rating);
        panel.add(ratingL);
        panel.add(proteinsL);
        panel.add(proteins);
        panel.add(fats);
        panel.add(fatsL);
        panel.add(sodiumL);
        panel.add(sodium);
        panel.add(priceL);
        panel.add(price);
        panel.add(search);
        panel.add(view);
        panel.add(view1);
        panel.add(makeOrder);

        this.add(panel);
    }
    public void viewButton(ActionListener actionListener) {
        this.view.addActionListener(actionListener);
    }
    public void view1Button(ActionListener actionListener) {
        this.view1.addActionListener(actionListener);
    }
    public void SearchButton(ActionListener actionListener) {
        this.search.addActionListener(actionListener);
    }
    public String getTitleJ()
    {
        return title.getText();
    }
    public String getRankJ()
    {
        return rating.getText();
    }
    public String getCaloriesJ()
    {
        return calories.getText();
    }
    public String getProteinsJ()
    {
        return proteins.getText();
    }
    public String getFatsJ()
    {
        return fats.getText();
    }
    public String getsodiumJ()
    {
        return sodium.getText();
    }

    public String getPriceJ()
    {
        return price.getText();
    }
    public void OrderButton(ActionListener actionListener) {
        this.makeOrder.addActionListener(actionListener);
    }

}
