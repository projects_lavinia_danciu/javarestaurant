package Presentation;

import Bussiness.BaseProduct;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class TableView<T> extends JFrame {
    private JTable table;
    private JScrollPane scroll;
    public TableView(List<T> List) {
        setTitle("View all");
        setSize(1400, 700);
        setLocationRelativeTo(null);
        Reflection<T> reflection=new Reflection<T>();
        String[] columns = {"Title", "Rating", "Calories", "Protein", "Fat", "Sodium", "Price"};
        String[][] rows = reflection.fetchRows(List);
        if(List.get(0) instanceof BaseProduct) table = new JTable(rows,columns);
        table.getTableHeader().setFont(new Font("Arial", Font.BOLD, 20));
        table.getTableHeader().setOpaque(true);
        table.getTableHeader().setBackground(new Color(255,136,203));
        table.setRowHeight(30);
        scroll = new JScrollPane(table);
        add(scroll);
        setVisible(true);
    }

}
