package DataLayer;

import Bussiness.BaseProduct;
import Bussiness.Client;
import Bussiness.CompositeProduct;
import Bussiness.DeliveryService;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public  class Serializator {
    DeliveryService deliveryService;

    public Serializator(DeliveryService deliveryService) {
        this.deliveryService = deliveryService;

    }

    public void SerializareBase() {
        try {
            FileOutputStream file = new FileOutputStream("text.ser");
            ObjectOutputStream out = new ObjectOutputStream(file);
            // Method for serialization of object
            List<BaseProduct> item=deliveryService.getListBaseProducts();
            out.writeObject(item);
            out.close();
            file.close();
            System.out.println("Object has been serialized");

        } catch (IOException ex) {
            System.out.println("IOException is caught");
        }

    }

    public List<BaseProduct> DeserializareBase() {
        List<BaseProduct> animalList = null;
        try {
            FileInputStream file = new FileInputStream("text.ser");
            ObjectInputStream in = new ObjectInputStream(file);
            animalList = (List<BaseProduct>) in.readObject(); // casting object
            in.close();
            file.close();
        } catch (EOFException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return  animalList;
    }


    public void SerializareComposed() {
        try {
            FileOutputStream file = new FileOutputStream("textComposed.ser");
            ObjectOutputStream out = new ObjectOutputStream(file);
            // Method for serialization of object
            List<CompositeProduct> item=deliveryService.getCompositelistProducts();
            out.writeObject(item);
            out.close();
            file.close();
            System.out.println("Object has been serialized");

        } catch (IOException ex) {
            System.out.println("IOException is caught");
        }

    }

    public List<CompositeProduct> DeserializareComposed() {
        List<CompositeProduct> animalList = null;
        try {
            FileInputStream file = new FileInputStream("textComposed.ser");
            ObjectInputStream in = new ObjectInputStream(file);
            animalList = (List<CompositeProduct>) in.readObject(); // casting object
            in.close();
            file.close();
        } catch (EOFException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return animalList;
    }
    public void SerializareClienti() {
        try {
            FileOutputStream file1 = new FileOutputStream("clients.ser");
            ObjectOutputStream out = new ObjectOutputStream(file1);
            // Method for serialization of object
            List<Client> item=deliveryService.getClientsList();
            out.writeObject(item);
            out.close();
            file1.close();
            System.out.println("Object Clients has been serialized");

        } catch (IOException ex) {
            System.out.println("IOException is caught");
        }

    }
    public List<Client> DeserializareClients() {
        LinkedList<Client> list = null;
        try {
            FileInputStream file1 = new FileInputStream("clients.ser");
            ObjectInputStream in = new ObjectInputStream(file1);
            list = (LinkedList<Client>) in.readObject(); // casting object
            in.close();
            file1.close();
        } catch (EOFException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return list;
    }



    public void flush(String name) {
        try {
            FileWriter fileWriter = new FileWriter(name);
            fileWriter.flush();
            fileWriter.close();
            System.out.println("Data Written to the file successfully");
        } catch (Exception e) {
            System.out.println("Error: " + e.toString());
        }
    }
}
