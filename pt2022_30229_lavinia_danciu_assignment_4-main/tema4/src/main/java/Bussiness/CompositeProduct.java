package Bussiness;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CompositeProduct implements MenuItem, Serializable {
    private String title;
    private List<BaseProduct> products;
    private float price;
    private int count;

    public CompositeProduct(String title, float price){
        this.title = title;
        this.price = price;
        this.count=0;
        this.products = new ArrayList<>();
    }
    public String showTable(){
        return "title: " + ", pret: " + price +" numbered ordered " +count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }


    public void addProduct(BaseProduct x ){
        this.products.add(x);
    }


    public float getPrice() {
        return price;
    }

    public List<BaseProduct> getProductss() {
        return products;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float computePrice(){
        int price = 0;
        for(MenuItem item : products){
            price += item.computePrice();
        }
        return price;
    }

    public String toString(){
        String print = "";
        for(BaseProduct b : this.products){
            print = print + b.getTitle() + ",\n";
        }
        return print ;
    }

    public String getTitle() {
        return title;
    }


    @Override
    public int compareTo(MenuItem o) {

        return this.getTitle().compareTo(o.getTitle());

    }
}
