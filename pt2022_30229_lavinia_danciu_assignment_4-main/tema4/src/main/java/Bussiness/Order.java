package Bussiness;


public class Order implements Observer {

    private static int orderID=0;
    private int idClient;
    private String orderDate;
    private String orderTime;
    private float orderPrice;
    private int idC;


    public Order(String data, String ora){

        this.orderPrice=0;
        this.idC=orderID;
        this.orderDate=data;
        this.orderTime=ora;
        this.orderID++;



    }
    public int getIdC() {
        return idC;
    }

    public void setIdC(int idC) {
        this.idC = idC;
    }


    public float getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(float orderPrice) {
        this.orderPrice = orderPrice;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    @Override
    public int hashCode(){
        return this.idC;
    }



    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getOrderDate() {
        return orderDate;
    }


    @Override
    public String update(int id,float price,String date,String time) {
        String res="";
        res="S-a plasat comanda cu numarul "+id+" pretul "+price + " data comanda " + date +" "+time+"\n" ;
        return res;
    }
}

