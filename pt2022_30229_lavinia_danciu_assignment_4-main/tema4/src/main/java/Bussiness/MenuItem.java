package Bussiness;


import java.io.Serializable;

public interface MenuItem extends Comparable<MenuItem>, Serializable {

    public float computePrice();
    public String getTitle();
    public float getPrice();
    public void setCount(int count);
    public int getCount();


}
