package Bussiness;

import javax.swing.*;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public interface IDeliveryServiceProcessing {

    public LinkedList<BaseProduct> importProduct() throws IOException;
    public void editProduct (BaseProduct product, String title, String rating, String calories, String protein, String fat, String sodium, String price);
    public List<BaseProduct> searchByCriteria(String title, String rating, String calories, String protein, String fat, String sodium, String price);
    public void Report2(int count, JTextArea area);
    public void Report3(int idc,int suma,int count,JTextArea area);
    public void Report4(String data,JTextArea text,int number);
    public void Report1(String start, String end,JTextArea area);
}