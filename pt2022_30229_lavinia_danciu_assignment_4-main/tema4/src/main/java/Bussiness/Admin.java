package Bussiness;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public  class Admin extends User
{
    public Admin(String username, String password, String name, String address) {
        super(username, password, name, address);
        this.setRole("ADMIN");
    }

    public LinkedList<BaseProduct> importProduct() throws IOException {
        Path path = Path.of("C:\\Users\\asus\\Desktop\\tema4\\target\\classes\\Bussiness\\products.csv");
        Pattern pattern = Pattern.compile(",");
        Set<BaseProduct> list;
        TreeSet<BaseProduct> treeSet = new TreeSet<>();
        try (Stream<String> lines = Files.lines(path)) {
            list = lines.skip(1).map(line -> {
                String[] arr = pattern.split(line);
                return new BaseProduct(arr[0], Float.parseFloat(arr[1]), Float.parseFloat(arr[2]), Float.parseFloat(arr[3]), Float.parseFloat(arr[4]), Float.parseFloat(arr[5]), Float.parseFloat(arr[6]));
            }).collect(Collectors.toSet());
            for(BaseProduct e:list) treeSet.add(e);}
        return new LinkedList(treeSet);
    }

    public void editProduct (BaseProduct product, String title, String rating, String calories, String protein, String fat, String sodium, String price){
        if(title.equals("")==false) product.setTitle(title);
        if(rating.equals("")==false) product.setRating(Float.parseFloat(rating));
        if(calories.equals("")==false) product.setCalories(Float.parseFloat(calories));
        if(protein.equals("")==false)  product.setProtein(Float.parseFloat(protein));
        if(fat.equals("")==false)   product.setFat(Float.parseFloat(fat));
        if(sodium.equals("")==false)   product.setSodium(Float.parseFloat(sodium));
        if(price.equals("")==false)  product.setPrice(Float.parseFloat(price));

    }


}


