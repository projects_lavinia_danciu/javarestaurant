package Bussiness;

import java.io.Serializable;

public class Client extends User implements Serializable
{
    private int idClient;


    public Client(String username, String password, String name, String address, int idClient) {
        super(username, password, name, address);
        this.setRole("CLIENT");
        this.idClient=idClient;
    }
    public int getIdClient() {
    return idClient;
}

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }
}
