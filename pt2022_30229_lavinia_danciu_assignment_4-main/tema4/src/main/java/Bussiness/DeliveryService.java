package Bussiness;

import javax.swing.*;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DeliveryService implements IDeliveryServiceProcessing, Serializable {
    private LinkedList<MenuItem> menuItems;
    private  LinkedList<Client> ClientsList;
    private  LinkedList<BaseProduct> listBaseProducts;
    private LinkedList<CompositeProduct> compositelistProducts;
    private  HashMap<Order, List<MenuItem>> orders=new HashMap<>();;
    public DeliveryService() {
        ClientsList = new LinkedList<>();
        listBaseProducts = new LinkedList<>();
        compositelistProducts = new LinkedList<>();
        menuItems = new LinkedList<>();
    }
    public HashMap<Order, List<MenuItem>> getOrders() {
        return orders;
    }

    public void setOrders(HashMap<Order, List<MenuItem>> orders) {
        this.orders = orders;
    }
    public   List<MenuItem> getMenuItems() {
            return menuItems;
            }

    public BaseProduct searchProduct(String menuItemName) {

        for (BaseProduct index : listBaseProducts) {
            if (index.getTitle().equals(menuItemName) == true)
                return index;
        }
        return null;
    }
    public MenuItem searchProductMenu(String menuItemName) {

        for (MenuItem index :menuItems) {
            if (index.getTitle().equals(menuItemName) == true)
                return index;
        }
        return null;
    }
    public int  searchMenuName(String menuItemName) {
        int ok = 0;
        for (CompositeProduct index : compositelistProducts) {
            if (index.getTitle().equals(menuItemName) == true)
                return 1;
        }
        return ok;
    }

    public MenuItem checkMenuName(String menuItemName)
    {
        for (CompositeProduct index : compositelistProducts) {
            if (index.getTitle().equals(menuItemName) == true)
                return  index;
        }
        for (BaseProduct index : listBaseProducts) {
            if (index.getTitle().equals(menuItemName) == true)
                return  index;
        }
        return null;
    }

    public LinkedList<Client> getClientsList() {
        return ClientsList;
    }
    public LinkedList<BaseProduct> getListBaseProducts() {
        return listBaseProducts;
    }

    public LinkedList<CompositeProduct> getCompositelistProducts() {
        return compositelistProducts;
    }
    public LinkedList<BaseProduct> importProduct() throws IOException {
        Path path = Path.of("C:\\Users\\asus\\Desktop\\tema4\\target\\classes\\products.csv");
        Pattern pattern = Pattern.compile(",");
        Set<BaseProduct> list;
        TreeSet<BaseProduct> treeSet = new TreeSet<>();
        try (Stream<String> lines = Files.lines(path)) {
            list = lines.skip(1).map(line -> {
                String[] arr = pattern.split(line);
                return new BaseProduct(arr[0], Float.parseFloat(arr[1]), Float.parseFloat(arr[2]), Float.parseFloat(arr[3]), Float.parseFloat(arr[4]), Float.parseFloat(arr[5]), Float.parseFloat(arr[6]));
            }).collect(Collectors.toSet());
            for(BaseProduct e:list) treeSet.add(e);}
        return new LinkedList(treeSet);
    }
    public void editProduct (BaseProduct product, String title, String rating, String calories, String protein, String fat, String sodium, String price){
        if(title.equals("")==false) product.setTitle(title);
        if(rating.equals("")==false) product.setRating(Float.parseFloat(rating));
        if(calories.equals("")==false) product.setCalories(Float.parseFloat(calories));
        if(protein.equals("")==false)  product.setProtein(Float.parseFloat(protein));
        if(fat.equals("")==false)   product.setFat(Float.parseFloat(fat));
        if(sodium.equals("")==false)   product.setSodium(Float.parseFloat(sodium));
        if(price.equals("")==false)  product.setPrice(Float.parseFloat(price));

    }
    public List<BaseProduct> searchByCriteria( String title, String rating, String calories, String protein, String fat, String sodium, String price){
        List<BaseProduct> products = this.listBaseProducts;
        List<BaseProduct> Data = products.stream().toList();
        if(title.isEmpty()==false) {
            Data =Data.stream().filter(b ->  b.getTitle().contains(title)).collect(Collectors.toList());
        }
        if(rating.isEmpty()==false) {
            Data =Data.stream().filter(b ->  String.valueOf(b.getRating()).startsWith(rating)).collect(Collectors.toList());
        }
        if(calories.isEmpty()==false) {
            Data =Data.stream().filter(b ->  String.valueOf(b.getCalories()).equals(calories)).collect(Collectors.toList());
        }
        if(protein.isEmpty()==false) {
            Data = Data.stream().filter(b -> String.valueOf(b.getProtein()).equals(protein)).collect(Collectors.toList());
        }
        if(fat.isEmpty()==false) {
            Data = Data.stream().filter(b -> String.valueOf(b.getFat()).equals(fat)).collect(Collectors.toList());
        }
        if(sodium.isEmpty()==false) {
            Data = Data.stream().filter(b -> String.valueOf(b.getSodium()).equals(sodium)).collect(Collectors.toList());
        }
        if(price.isEmpty()==false) {
            Data = Data.stream().filter(b -> String.valueOf(b.getPrice()).equals(price)).collect(Collectors.toList());
        }
        return Data;
    }
    public void  OrderAdd(Order order, List<MenuItem> products) {
        float price = 0;
        for(MenuItem index : products){
           price=price+index.getPrice();
        }
        order.setOrderPrice(price);
        this.orders.put(order,products);
        try{
            FileWriter writeAux=new FileWriter("C:\\Users\\asus\\Desktop\\tema4\\Bill"+order.getOrderID()+".txt");
            writeAux.write("id client "+order.getIdClient()+" id comanda "+order.getOrderID()+" price \n"+order.getOrderPrice()+ " date " + order.getOrderDate()+ " time "+order.getOrderTime()+"\n");
            List<MenuItem> listaProduse=new LinkedList<>();
            for(Order index:orders.keySet())
            {
                 if(index.getIdC()==order.getIdC())
                 {
                  listaProduse=orders.get(index);
                 }
                for(MenuItem m: listaProduse)
                {
                    writeAux.write(m.getTitle()+" , ");
                }
            }
            writeAux.close();
        }catch(IOException e) {
            e.printStackTrace();
        }
    }
    public void Report1(String start, String end,JTextArea area)
    {     String rezult="";
           Set<Order>list=orders.keySet();
           List<Order> generated=list.stream()
                   .filter(o->o.getOrderTime().compareTo(start)>=0 && o.getOrderTime().compareTo(end)<=0)
              .collect(Collectors.toList());

        for(Order index: generated) {
            rezult = " id comanda " + index.getIdC() + " date " + index.getOrderDate() + " time " + index.getOrderTime() + " price "+ index.getOrderPrice()+"\n";
            area.append(rezult);
        }
    }
    public void Report2(int count, JTextArea area) {

        String rezult="";
        Set<MenuItem> generatedProducts = null;
        for (MenuItem o : menuItems) {
            generatedProducts = menuItems.stream()
                    .filter(p -> p.getCount() > count)
                    .collect(Collectors.toSet());
        }
        for(MenuItem index:generatedProducts) {
            rezult = "name " + index.getTitle() + " price " + String.valueOf(index.getPrice()) + " total ordered " + String.valueOf(index.getCount()) + "\n";
            area.append(rezult);
        }

    }
    public void Report3(int idc,int suma,int count,JTextArea area) {
        Set<Order> list = orders.keySet();
        Set<Order> listGenerated = list.stream()
                .filter(o -> o.getOrderPrice() > suma && o.getIdClient()==idc )
                .collect(Collectors.toSet());
       String result="";
        if(listGenerated.size()>count)
        {    for(Order index:listGenerated) {
             result = "id client " + idc + " total ordered " + String.valueOf(index.getOrderPrice()) + "\n";
             area.append(result);
        }
        }
    }
    public int searchClient(List<Client> clients,String username,String password) {
        for (Client index : clients) {
            if (index.getUsername().equals(username) && index.getPassword().equals(password))
                return index.getIdClient();
        }
        return 0;
    }
    public void Report4(String data,JTextArea text,int number) {
        Set<Order> list = orders.keySet();

        Collection<List<MenuItem>> items = orders.values();
        List<MenuItem>res=new LinkedList<MenuItem>();
        List<MenuItem>finalList=new LinkedList<MenuItem>();
        List<Order> listIterator= list.stream()
                .filter(o -> o.getOrderDate().compareTo(data)==0 )
                .collect(Collectors.toList());
        for (Order index:listIterator)
       {  for(MenuItem aux:orders.get(index))
          {res.add(aux);}
       }
        List<MenuItem> resDistinct= res.stream().distinct()
                .collect(Collectors.toList());

        System.out.println("AFISARE");
        for(MenuItem id:resDistinct)
            System.out.println(id.getTitle());
        int cont=0;
        String result="";
        for(MenuItem i:resDistinct)
        {   cont=0;
            System.out.println("iul " +i);
            for(MenuItem j:res)
            {  System.out.println("jul " +j);
                if(i.equals(j)) cont++;
                System.out.println(i.getTitle());
            }

            if(cont==number) {
                result = "data" + i.getTitle()+ "total ordered " + number+"\n";
                text.append(result);
            }}}
}

