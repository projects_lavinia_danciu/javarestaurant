package Bussiness;


import java.io.Serializable;

public class BaseProduct implements MenuItem, Serializable {
    private String title;
    private float rating;
    private float calories;
    private float protein;
    private float fat;
    private float sodium;
    private float price;
    private int count=0;

    public BaseProduct(String title, float rating, float calories, float protein, float fat, float sodium, float price){
        this.title = title;
        this.rating = rating;
        this.calories = calories;
        this.protein = protein;
        this.fat = fat;
        this.sodium = sodium;
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String toString(){
        return "title: " + title + ", rating: " + rating + ", calorii: " + calories + ", pret: " + price;
    }
    public String showTable(){
        return "title: " + ", pret: " + price +"numbered ordered" +count;
    }

    public String getTitle() {
        return title;
    }
    public float computePrice() {
        return  price;
    }


    public float getRating() {
        return rating;
    }

    public float getCalories() {
        return calories;
    }

    public float getProtein() {
        return protein;
    }

    public float getFat() {
        return fat;
    }

    public float getSodium() {
        return sodium;
    }

    public float getPrice() {
        return price;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public void setCalories(float calories) {
        this.calories = calories;
    }

    public void setProtein(float protein) {
        this.protein = protein;
    }

    public void setFat(float fat) {
        this.fat = fat;
    }

    public void setSodium(float sodium) {
        this.sodium = sodium;
    }

    public void setPrice(float price) {
        this.price = price;
    }


    @Override
    public int compareTo(MenuItem o) {

        return this.getTitle().compareTo(o.getTitle());

    }
}
